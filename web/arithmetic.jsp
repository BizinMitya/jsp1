<%--
  Created by IntelliJ IDEA.
  User: Dmitriy
  Date: 03.04.2016
  Time: 19:38
  To change this template use File | Settings | File Templates.
--%>
<%@page contentType="text/html;charset=UTF-8" language="java" %>
<%@page errorPage="error.jsp" %>
<html>
<head>
    <title>Калькулятор</title>
</head>
<body>
<%
    String string1 = request.getParameter("operands1");
    String string2 = request.getParameter("operands2");
    double operands1 = 0;
    double operands2 = 0;
    if (string1 != null && string2 != null) {
        operands1 = Double.parseDouble(string1);
        operands2 = Double.parseDouble(string2);
    } else {
        string1 = "";
        string2 = "";
    }
%>

<form action="arithmetic.jsp" method="POST">
    <p><b>Первое число:</b>
        <input type="text" name="operands1" value="<%=string1%>">
    </p>
    <p><b>Второе число:</b>
        <input type="text" name="operands2" value="<%=string2%>">
    </p>
    <input type="submit" name="sum" value="Сложить">
    <input type="submit" name="sub" value="Вычесть">
    <input type="submit" name="mul" value="Умножить">
    <input type="submit" name="div" value="Разделить">
</form>

<%
    if (request.getMethod().equalsIgnoreCase("POST")) {
        String sum = request.getParameter("sum");
        String sub = request.getParameter("sub");
        String mul = request.getParameter("mul");
        String div = request.getParameter("div");
        if (sum != null) {
            if (operands1 < 0 && operands2 < 0) {
                out.print("<p><b>" + "(" + String.valueOf(operands1) + ")" + " + " + "(" + String.valueOf(operands2) + ")" + " = " + (operands1 + operands2) + "</b></p>");
            } else if (operands1 < 0 && operands2 >= 0) {
                out.print("<p><b>" + "(" + String.valueOf(operands1) + ")" + " + " + String.valueOf(operands2) + " = " + (operands1 + operands2) + "</b></p>");
            } else if (operands2 < 0 && operands1 >= 0) {
                out.print("<p><b>" + String.valueOf(operands1) + " + " + "(" + String.valueOf(operands2) + ")" + " = " + (operands1 + operands2) + "</b></p>");
            } else if (operands1 >= 0 && operands2 >= 0) {
                out.print("<p><b>" + String.valueOf(operands1) + " + " + String.valueOf(operands2) + " = " + (operands1 + operands2) + "</b></p>");
            }
        }
        if (sub != null) {
            if (operands1 < 0 && operands2 < 0) {
                out.print("<p><b>" + "(" + String.valueOf(operands1) + ")" + " - " + "(" + String.valueOf(operands2) + ")" + " = " + (operands1 - operands2) + "</b></p>");
            } else if (operands1 < 0 && operands2 >= 0) {
                out.print("<p><b>" + "(" + String.valueOf(operands1) + ")" + " - " + String.valueOf(operands2) + " = " + (operands1 - operands2) + "</b></p>");
            } else if (operands2 < 0 && operands1 >= 0) {
                out.print("<p><b>" + String.valueOf(operands1) + " - " + "(" + String.valueOf(operands2) + ")" + " = " + (operands1 - operands2) + "</b></p>");
            } else if (operands1 >= 0 && operands2 >= 0) {
                out.print("<p><b>" + String.valueOf(operands1) + " - " + String.valueOf(operands2) + " = " + (operands1 - operands2) + "</b></p>");
            }
        }
        if (mul != null) {
            if (operands1 < 0 && operands2 < 0) {
                out.print("<p><b>" + "(" + String.valueOf(operands1) + ")" + " * " + "(" + String.valueOf(operands2) + ")" + " = " + (operands1 * operands2) + "</b></p>");
            } else if (operands1 < 0 && operands2 >= 0) {
                out.print("<p><b>" + "(" + String.valueOf(operands1) + ")" + " * " + String.valueOf(operands2) + " = " + (operands1 * operands2) + "</b></p>");
            } else if (operands2 < 0 && operands1 >= 0) {
                out.print("<p><b>" + String.valueOf(operands1) + " * " + "(" + String.valueOf(operands2) + ")" + " = " + (operands1 * operands2) + "</b></p>");
            } else if (operands1 >= 0 && operands2 >= 0) {
                out.print("<p><b>" + String.valueOf(operands1) + " * " + String.valueOf(operands2) + " = " + (operands1 * operands2) + "</b></p>");
            }
        }
        if (div != null) {
            if (operands1 < 0 && operands2 < 0) {
                out.print("<p><b>" + "(" + String.valueOf(operands1) + ")" + " / " + "(" + String.valueOf(operands2) + ")" + " = " + (operands1 / operands2) + "</b></p>");
            } else if (operands1 < 0 && operands2 >= 0) {
                out.print("<p><b>" + "(" + String.valueOf(operands1) + ")" + " / " + String.valueOf(operands2) + " = " + (operands1 / operands2) + "</b></p>");
            } else if (operands2 < 0 && operands1 >= 0) {
                out.print("<p><b>" + String.valueOf(operands1) + " / " + "(" + String.valueOf(operands2) + ")" + " = " + (operands1 / operands2) + "</b></p>");
            } else if (operands1 >= 0 && operands2 >= 0) {
                if (operands2 == 0) {
                    throw new Exception("Деление на 0!");
                }
                out.print("<p><b>" + String.valueOf(operands1) + " / " + String.valueOf(operands2) + " = " + (operands1 / operands2) + "</b></p>");
            }
        }
    }
%>
</body>
</html>
